The course has been good so far, it pushed me to start learning about git.
I already feel more confident when contributing to team project.

I think it is hard to teach a class where you have people on different level of 
using/knowledge of git, so my only recommendation for the next run would be to try
split people into seminar groups more evenly, if it is possible of course. 
